# Création d'un microservice RESTful avec Spring Data REST <br><br>


## Pile technologique <br><br>

* Java 8
* Spring Data REST
* Maven 
* Swagger (OPEN API)
* JPA H2

## Compilation / Execution <br><br>

```
git clone
mvn clean install
java -jar target/springDataRest-0.1.0.jar
```

## Utilisation <br><br>

se rendre à http://localhost:8080/swagger-ui.html pour accèder à la documentation de l'API RESTful