package fr.malakoffmederic.springdatarest.repository;

/**
 *
 * @author Victor BANVILLET
 */
import java.util.List;
import fr.malakoffmederic.springdatarest.entity.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "people", path = "people")
public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {

	List<Person> findByStatutLibelle(@Param("libelle") String libelle);

        @Query("SELECT p from Person p inner join fetch p.statut s where s.libelle = :libelle")
        List<Person> exempleDeRequeteCompliquee(@Param("libelle") String libelle);
}