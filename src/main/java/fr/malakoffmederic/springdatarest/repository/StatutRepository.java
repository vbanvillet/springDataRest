package fr.malakoffmederic.springdatarest.repository;

/**
 *
 * @author Victor BANVILLET
 */
import fr.malakoffmederic.springdatarest.entity.Statut;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "statut", path = "statut")
public interface StatutRepository extends PagingAndSortingRepository<Statut, Long> {

}